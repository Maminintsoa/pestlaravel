<?php

use App\Mail\UpdateBookingMail;
use App\Models\Booking;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

uses(TestCase::class, RefreshDatabase::class, WithFaker::class);

it('mail_is_generated_for_updated_booking', function () {
    // Arrange
    $booking = Booking::factory()->create([
        'name' => $this->faker->name,
        'email' => $this->faker->email,
        'phone' => $this->faker->phoneNumber,
        'date' => $this->faker->date,
        'time' => $this->faker->time,
    ]);

    Mail::fake();

    $updatedTime = $this->faker->time;

    $booking->update(['time' => $updatedTime]);

    // Render the email view with the updated booking
    $emailView = View::make('emails.update_booking', ['booking' => $booking])->render();

    // Act
    Mail::to($booking->email)->send(new UpdateBookingMail($booking));

    // Assert
    Mail::assertSent(UpdateBookingMail::class, function ($mail) use ($booking, $emailView) {
        return $mail->hasTo($booking->email)
            && $mail->subject('Booking Update Confirmation')
            && strpos($mail->render(), $emailView) !== false;
    });
})->group('update');