<?php

use App\Mail\CustomReminderMail;
use App\Models\Booking;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

uses(TestCase::class, RefreshDatabase::class);

it('mail_is_generated_as_custom_reminder', function () {
    // Arrange
    $booking = Booking::factory()->create();

    Mail::fake();

    // Act
    Mail::to($booking->email)->send(new CustomReminderMail($booking, 'Custom Subject'));

    // Assert
    Mail::assertSent(CustomReminderMail::class, function ($mail) use ($booking) {
        return $mail->hasTo($booking->email)
            && $mail->subject('Custom Subject')
            && $mail->booking->id === $booking->id
            && $mail->booking->time === $booking->time;
    });
})->group('custom_reminder');
