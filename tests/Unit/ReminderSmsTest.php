<?php

use App\Models\Booking;
use App\Services\SmsService;
use App\Notifications\ReminderSmsNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

uses(TestCase::class, RefreshDatabase::class);

it('sms_is_generated_as_reminder_for_booking', function () {
    // Arrange
    $booking = Booking::factory()->create();

    Notification::fake();

    // Act
    $booking->notify(new ReminderSmsNotification($booking));

    // Assert
    Notification::assertSentTo(
        $booking,
        ReminderSmsNotification::class,
        function ($notification, $channels, $notifiable) use ($booking) {
            return $notifiable->id === $booking->id;
        }
    );
})->group('reminder');