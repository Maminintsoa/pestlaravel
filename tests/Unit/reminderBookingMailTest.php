<?php

use App\Mail\ReminderBookingMail;
use App\Models\Booking;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

uses(TestCase::class, RefreshDatabase::class);

it('mail_is_generated_as_reminder', function () {
    // Arrange
    $booking = Booking::factory()->create();

    Mail::fake();

    // Act
    Mail::to($booking->email)->send(new ReminderBookingMail($booking));

    // Assert
    Mail::assertSent(ReminderBookingMail::class, function ($mail) use ($booking) {
        return $mail->hasTo($booking->email)
            && $mail->subject('Booking Reminder')
            && $mail->booking->id === $booking->id
            && $mail->booking->time === $booking->time;
    });
})->group('reminder');