<?php

use App\Mail\NewBookingMail;
use App\Models\Booking;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(Tests\TestCase::class, RefreshDatabase::class);

it('generates a mail for a new booking', function () {
    // Arrange
    $booking = Booking::factory()->create();

    Mail::fake();

    // Act
    Mail::to($booking->email)->send(new NewBookingMail($booking));

    // Assert
    Mail::assertSent(NewBookingMail::class, function ($mail) use ($booking) {
        $mailContent = $mail->render();

        return $mail->hasTo($booking->email)
            && $mail->subject === 'New Booking Confirmation'
            && strpos($mailContent, $booking->name) !== false
            && strpos($mailContent, $booking->phone) !== false
            && strpos($mailContent, $booking->date) !== false
            && strpos($mailContent, $booking->time) !== false;
    });
})->group('new');