<?php

use App\Models\Booking;
use App\Services\SmsService;
use App\Notifications\NewBookingSmsNotification;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;

uses(Tests\TestCase::class, RefreshDatabase::class);

it('generates an SMS for a new booking', function () {
    // Arrange
    $booking = Booking::factory()->create();

    Notification::fake();

    // Act
    $booking->notify(new NewBookingSmsNotification($booking));

    // Assert
    Notification::assertSentTo(
        $booking,
        NewBookingSmsNotification::class,
        function ($notification, $channels, $notifiable) use ($booking) {
            return $notifiable->id === $booking->id;
        }
    );
})->group('new');