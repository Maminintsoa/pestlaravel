<?php

use App\Models\Booking;
use App\Services\SmsService;
use App\Notifications\UpdateBookingSmsNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

uses(TestCase::class, RefreshDatabase::class, WithFaker::class);

it('sms_is_generated_for_updated_booking', function () {
    // Arrange
    $booking = Booking::factory()->create([
        'name' => $this->faker->name,
        'email' => $this->faker->email,
        'phone' => $this->faker->phoneNumber,
        'date' => $this->faker->date,
        'time' => $this->faker->time,
    ]);

    $updatedTime = $this->faker->time;

    Notification::fake();

    // Act
    $booking->update(['time' => $updatedTime]);
    $booking->notify(new UpdateBookingSmsNotification($booking));

    // Assert
    Notification::assertSentTo(
        $booking,
        UpdateBookingSmsNotification::class,
        function ($notification, $channels, $notifiable) use ($booking) {
            return $notifiable->id === $booking->id;
        }
    );
})->group('update');