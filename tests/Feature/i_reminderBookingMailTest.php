<?php

use App\Mail\ReminderBookingMail;
use App\Models\Booking;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

it('mail_is_generated_as_reminder - integration', function () {
    // Arrange
    Mail::fake();
    $booking = Booking::factory()->create();
    $user = User::factory()->create();
    $this->actingAs($user);
    $token = csrf_token();

    // Act
    $response = $this->post('/bookings', [
        'name' => $booking->name,
        'email' => $booking->email,
        'phone' => $booking->phone,
        'date' => $booking->date,
        'time' => $booking->time,
    ], ['X-CSRF-Token' => $token]);

    // Assert
    $response->assertSuccessful();

    $reminderMail = new ReminderBookingMail($booking);

    // Act
    Mail::to($booking->email)->send($reminderMail);

    // Assert
    Mail::assertSent(ReminderBookingMail::class, function ($mail) use ($booking) {
        return $mail->hasTo($booking->email)
            && $mail->subject('Booking Reminder')
            && $mail->booking->id === $booking->id
            && $mail->booking->time === $booking->time;
    });
})->group('i_reminder');
