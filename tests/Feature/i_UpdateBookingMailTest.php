<?php

use App\Mail\UpdateBookingMail;
use App\Models\Booking;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Testing\WithFaker;

uses(WithFaker::class);

it('mail_is_generated_for_updated_booking - integration', function () {
     // Arrange
    $booking = Booking::factory()->create([
        'name' => $this->faker->name,
        'email' => $this->faker->email,
        'phone' => $this->faker->phoneNumber,
        'date' => $this->faker->date,
        'time' => $this->faker->time,
    ]);

    $newTime = $this->faker->time;;

    $updatedBooking = Booking::find($booking->id);
    $updatedBooking->time = $newTime;
    $updatedBooking->save();

    $emailView = View::make('emails.update_booking', ['booking' => $updatedBooking])->render();

    $updateBookingMail = new UpdateBookingMail($updatedBooking);

    // Act
    Mail::fake();
    Mail::to($updatedBooking->email)->send($updateBookingMail);

    // Assert
    Mail::assertSent(UpdateBookingMail::class, function ($mail) use ($updatedBooking, $emailView) {
        return $mail->hasTo($updatedBooking->email)
            && $mail->subject('Booking Update Confirmation')
            && strpos($mail->render(), $emailView) !== false;
    });
    
    $this->assertEquals($newTime, $updatedBooking->fresh()->time);
})->group('i_update');