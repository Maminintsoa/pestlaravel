<?php

use App\Models\Booking;
use App\Services\SmsService;
use App\Notifications\NewBookingSmsNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;

it('generates an SMS for a new booking - integration', function () {
    // Arrange
    $booking = Booking::factory()->create();
    Notification::fake();

    // Act
    $foundBooking = Booking::find($booking->id);
    $this->assertEquals($booking->id, $foundBooking->id);

    $updatedBooking = Booking::find($booking->id);
    $updatedBooking->save();

    $booking->notify(new NewBookingSmsNotification($booking));

    // Assert
    Notification::assertSentTo(
        $booking,
        NewBookingSmsNotification::class,
        function ($notification, $channels, $notifiable) use ($booking) {
            return $notifiable->id === $booking->id;
        }
    );
})->group('i_new');