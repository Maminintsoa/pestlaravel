<?php

use App\Models\Booking;
use App\Services\SmsService;
use App\Notifications\ReminderSmsNotification;
use Illuminate\Support\Facades\Notification;

it('sms_is_generated_as_reminder_for_booking - integration', function () {
    // Arrange
    $this->seed();

    $bookingData = [
        'name' => fake()->name,
        'email' => fake()->email,
        'phone' => fake()->phoneNumber,
        'date' => fake()->date,
        'time' => fake()->time,
    ];

    $this->post('/bookings', $bookingData);

    $booking = Booking::where($bookingData)->first();

    Notification::fake();

    // Act
    $booking->notify(new ReminderSmsNotification($booking));

    // Assert
    Notification::assertSentTo(
        $booking,
        ReminderSmsNotification::class,
        function ($notification, $channels, $notifiable) use ($booking) {
            return $notifiable->id === $booking->id;
        }
    );
})->group('i_reminder');