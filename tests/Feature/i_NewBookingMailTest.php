<?php

use App\Mail\NewBookingMail;
use App\Models\Booking;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

it('generates a mail for a new booking - integration', function () {
    // Arrange
    Mail::fake();
    $booking = Booking::factory()->create();
    $user = User::factory()->create();
    $this->actingAs($user);
    $token = csrf_token();

    // Act
    $response = $this->post('/bookings', [
        'name' => $booking->name,
        'email' => $booking->email,
        'phone' => $booking->phone,
        'date' => $booking->date,
        'time' => $booking->time,
    ], ['X-CSRF-Token' => $token]);

    // Assert
    $response->assertSuccessful();

    Mail::assertSent(NewBookingMail::class, function ($mail) use ($booking) {
        $mailContent = $mail->render();

        return $mail->hasTo($booking->email) &&
            $mail->subject === 'New Booking Confirmation' &&
            strpos($mailContent, $booking->name) !== false &&
            strpos($mailContent, $booking->phone) !== false &&
            strpos($mailContent, $booking->date) !== false &&
            strpos($mailContent, $booking->time) !== false;
    });
})->group('i_new');