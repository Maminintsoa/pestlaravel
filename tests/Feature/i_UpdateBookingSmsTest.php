<?php

use App\Models\Booking;
use App\Services\SmsService;
use App\Notifications\UpdateBookingSmsNotification;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;

uses(WithFaker::class);

it('sms_is_generated_for_updated_booking - integration', function () {
    // Arrange
    $this->seed();

    $booking = Booking::factory()->create([
        'name' => $this->faker->name,
        'email' => $this->faker->email,
        'phone' => $this->faker->phoneNumber,
        'date' => $this->faker->date,
        'time' => $this->faker->time,
    ]);
    
    Notification::fake();

    $updatedTime = $this->faker->time;
    $booking->update(['time' => $updatedTime]);

    $smsService = app(SmsService::class);
    $smsService->sendSms($booking->phone, 'Your booking has been updated.');

    $booking->notify(new UpdateBookingSmsNotification($booking));

    // Assert
    Notification::assertSentTo(
        $booking,
        UpdateBookingSmsNotification::class,
        function ($notification, $channels, $notifiable) use ($booking) {
            return $notifiable->id === $booking->id;
        }
    );
})->group('i_update');