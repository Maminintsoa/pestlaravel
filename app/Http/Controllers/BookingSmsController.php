<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BookingSmsController extends Controller {

    protected $smsService;

    public function __construct( SmsService $smsService ) {
        $this->smsService = $smsService;
    }

    public function store( Request $request ) {
        // Validate the request data
        $validatedData = $request->validate( [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string',
            'date' => 'required|date',
            'time' => 'required|string',
        ] );

        // Create a new booking record
        $booking = Booking::create( $validatedData );

        // Add this code to send the SMS notification
        $this->smsService->sendSms( $booking->phone, 'New booking: ' . $booking->id );

        // Return a success response
        return response()->json( [ 'message' => 'Booking created successfully' ], 200 );
    }
}
