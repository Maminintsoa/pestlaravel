<?php

namespace App\Http\Controllers;

use App\Mail\NewBookingMail;
use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BookingController extends Controller {
    public function store( Request $request ) {
        // Validate the request data
        $validatedData = $request->validate( [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string',
            'date' => 'required|date',
            'time' => 'required|string',
        ] );

        // Create a new booking record
        $booking = Booking::create( $validatedData );

        // Send the new booking email
        Mail::to( $booking->email )->send( new NewBookingMail( $booking ) );

        // Return a success response
        return response()->json( [ 'message' => 'Booking created successfully' ], 200 );
    }
}
