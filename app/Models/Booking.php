<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\Notifiable;
use App\Notifications\NewBookingSMSNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Mail\NewBookingMail;
use App\Services\SmsService;


class Booking extends Model
{
    use Notifiable;
    use HasFactory;
    
    protected $fillable = ['name', 'email', 'phone', 'date', 'time'];

    /**
     * Send a new booking notification.
     *
     * @return void
     */
    public function sendNewBookingNotification()
    {
        $this->notify(new NewBookingSmsNotification($this));
    }

    public function setSmsService(SmsService $smsService)
    {
        $this->smsService = $smsService;
    }

    


}
