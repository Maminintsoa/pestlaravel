<!DOCTYPE html>
<html>
<head>
    <title>Booking Update Confirmation</title>
</head>
<body>
    <h1>Booking Update Confirmation</h1>
    <p>Dear {{ $booking->name }},</p>
    <p>Your booking has been updated with the following details:</p>
    <ul>
        <li>Name: {{ $booking->name }}</li>
        <li>Email: {{ $booking->email }}</li>
        <li>Phone: {{ $booking->phone }}</li>
        <li>Date: {{ $booking->date }}</li>
        <li>Time: {{ $booking->time }}</li>
    </ul>
</body>
</html>
