<!DOCTYPE html>
<html>
<head>
    <title>New Booking Confirmation</title>
</head>
<body>
    <h1>New Booking Confirmation</h1>
    <p>Name: {{ $booking->name }}</p>
    <p>Email: {{ $booking->email }}</p>
    <p>Phone: {{ $booking->phone }}</p>
    <p>Date: {{ $booking->date }}</p>
    <p>Time: {{ $booking->time }}</p>
</body>
</html>
